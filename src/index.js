import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

var WebApplication = require('./App')

ReactDOM.render(
  <WebApplication />,
  document.getElementById('root')
);
