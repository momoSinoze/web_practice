import React from 'react';
import './SuggestedSongContainer.css';

var topChartSongs  = require('./json/topChartSongs')
var playlists  = require('./json/playlists')

//this is only a mock for pictures
var wData = [
  {bandPic: 'http://p1.isanook.com/jo/0/rp/rc/w165h165/ya0xac0m1w0/aHR0cDovL3AxLmlzYW5vb2suY29tL2pvLzAvdWQvNDcwLzIzNTQxMjkvYXRvbV8xLmpwZw==.jpg', bandName: 'Atom (อะตอม)'},
  {bandPic: 'http://p1.isanook.com/jo/0/rp/rc/w165h165/ya0xac0m1w0/aHR0cDovL3AxLmlzYW5vb2suY29tL2pvLzAvdWQvNDcwLzIzNTQxMTMvYm9keXNsYW0uanBn.jpg', bandName: 'Bodyslam'},
  {bandPic: 'http://p1.isanook.com/jo/0/rp/rc/w165h165/ya0xac0m1w0/aHR0cDovL3AxLmlzYW5vb2suY29tL2pvLzAvdWQvMTcvODUyNTcvcG9seWNhdC5qcGc=.jpg', bandName: 'Polycat'},
  {bandPic: 'http://p1.isanook.com/jo/0/rp/rc/w165h165/ya0xac0m1w0/aHR0cDovL3AxLmlzYW5vb2suY29tL2pvLzAvdWQvMTcvODUxMDUvbWljaGFlbGJ1YmxlLmpwZw==.jpg', bandName: 'Michael Bublé'},
  {bandPic: 'http://p1.isanook.com/jo/0/rp/rc/w165h165/ya0xac0m1w0/aHR0cDovL3AxLmlzYW5vb2suY29tL2pvLzAvdWQvMTYvODQ2NzcvYnJpdG5leXNwZWFycy5qcGc=.jpg', bandName: 'Britney Spears'},
  {bandPic: 'http://p1.isanook.com/jo/0/rp/rc/w165h165/ya0xac0m1w0/aHR0cDovL3AxLmlzYW5vb2suY29tL2pvLzAvdWQvMTYvODQ2MjUvYmV5b25jZS5qcGc=.jpg', bandName: 'Beyoncé'},
];

var SuggestedSongContainer = React.createClass({
  render: function() {
    return (
      <div className="topComponent">
        <div id="left-panel" className="col-8">
          <PictureSlide wData={wData} />
          <SuggestedPlaylists data={playlists}/>
        </div>

        <div id="right-panel" className="col-3">
          <TopChart data={topChartSongs}/>
        </div>
      </div>
    );
  }
});

var PictureSlide = React.createClass({
  getInitialState: function() {
    return {counter: 0};
  },
  myFunction: function(update){
    if(this.state.counter+update < 0){
      this.setState({counter:(this.props.wData.length - 1)});
    }
    else if(this.state.counter + update >= this.props.wData.length){
      this.setState({counter: 0});
    }else{
      this.setState({counter: this.state.counter+update});
    }
  },
  render: function() {
    return (
      <div className="picSlide">
        <BackButton updateCounter={this.myFunction} />
        Large Picture
        <img src={this.props.wData[this.state.counter].bandPic} />
        <ForwardButton updateCounter={this.myFunction}/>
      </div>
    );
  }
});
var BackButton = React.createClass({
  handleClick: function(){
    this.props.updateCounter(-1);
  },
  render: function() {
    return(
        <button onClick={this.handleClick}> &lt; </button>
    );
  }
});
var ForwardButton = React.createClass({
  handleClick: function(){
    this.props.updateCounter(1);
  },
  render: function() {
    return(
        <button onClick={this.handleClick}> &gt; </button>
    );
  }
});

var SuggestedPlaylists = React.createClass({
  render: function() {
    var playlists = this.props.data.map(function(playlist) {
      return (
        <Playlist data={playlist} key={playlist.id}/>
      );
    });
    return (
      <div className="playlist">
        {playlists}
      </div>
    );
  }
});
var Playlist = React.createClass({
  render: function() {
    return (
      <div className="eachPlaylist">
        <PlaylistPicture id={this.props.data.id} />
        <PlaylistText title={this.props.data.title} numOfSong={this.props.data.numOfSong} />
      </div>
    );
  }
});
var PlaylistPicture = React.createClass({
  render: function() {
    return (
      <div className="playlist_pic" >Picture {this.props.id}</div>
    );
  }
});
var PlaylistText = React.createClass({
  render: function() {
    return (
      <div className = "playlist_text">
        <b>{this.props.title}</b> <br />
        {this.props.numOfSong}
      </div>
    );
  }
});

var TopChart = React.createClass({
  render: function() {
    var topSongList = this.props.data.map(function(song) {
      return (
        <Song artist={song.artist} title={song.title} key={song.id} />
      );
    });

    return (
      <div className="topChart">
        <b>JOOX TOP 100 CHART</b>
        {topSongList}
      </div>
    );
  }
});
var Song = React.createClass({
  render: function() {
    console.log("artist: " + this.props.artist);


    return (
      <div className="topSong">
        <div><b>{this.props.title}</b></div>
        <div>{this.props.artist}</div>
      </div>
    );
  }
});



module.exports = SuggestedSongContainer;
