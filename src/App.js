import React, { Component } from 'react';
import './App.css';
var SuggestedSongContainer  = require('./SuggestedSongContainer')

var WebApplication = React.createClass({
  render: function() {
    return (
      <div className="myWeb">
        <SuggestedSongContainer />
        <BottomComponent />
      </div>

    );
  }
});


var TopComponent = React.createClass({
  render: function() {
    return (
      <div className="topComponent">
        - Hello! I am a Top Component.
      </div>
    );
  }
});
var BottomComponent = React.createClass({
  render: function() {
    // <NewsBoard />
    // <Weeklyband />
    return (
      <div className="bottomComponent">

      <section className="section section--dark-solid">
      <div className=" wrapper">
      <div className="row">
      <ConcertDateBoard data={cData} />
      <NewsBoard data={nData}/>

      </div>
      </div>
      </section>
      <section className="section section--dark-transparent section--pd-bottom-lg">
      <div className="wrapper">
      <WeeklyBand data={wData}/>
      </div>
      </section>
      </div>
    );
  }
});

//Data for ConcertDateBoard
var cData = [
  {pic: 'http://p1.isanook.com/jo/0/rp/rc/w358h472/ya0xac0m1w0/aHR0cDovL3AxLmlzYW5vb2suY29tL2pvLzAvdWQvNDc1LzIzNzg5MTcvMjM3ODkxNy10aHVtYm5haWwuanBn.jpg', conName: "Queen + Adam Lambert On Tour in Bangkok", conDate: "30 กันยายน 2559"},
  {pic: '', conName: "WATERZONIC", conDate: "30 กันยายน 2559"},
  {pic: '', conName: "WET & WILD Festival 2016", conDate: "1 ตุลาคม 2559"},
  {pic: '', conName: "CHANG MUSIC CONNECTION PRESENTS THE GRANDSLAM LIVE BODYSLAM WITH THE ORCHESTRA", conDate: "1 ตุลาคม 2559"}
];
//ConcertDateBoard
var ConcertDateBoard = React.createClass({
  render: function() {
    var concerts = this.props.data.map(function(concert, i) {
      if(i == 0){
        return (
          <TopConcertpost pic={concert.pic} conName={concert.conName} conDate={concert.conDate} key={i}></TopConcertpost>
        );
      }
      else{
        return  (
          <Concertpost conName={concert.conName} conDate={concert.conDate} key={i}></Concertpost>
        );
      }
    });
    return (
      <div className="col span-4">
      <h2 className="event-widget__heading">
        คอนเสิร์ต
      </h2>
        {concerts}
        <a className="event-widget__more-btn">
        ดูปฏิทินทั้งหมด
        </a>
      </div>
    );
  }
});
var TopConcertpost = React.createClass({
  render: function() {
    return (
      <div className="event-widget__post event-widget__post--lg">
      <img src={this.props.pic} className="ConcertPic" alt="logo" />
      <h3 className="event-widget__title event-widget__title--xlg">
      {this.props.conName}
      </h3>
      <time className="event-widget__date">
      {this.props.conDate}
      </time>
      </div>
    );
  }
});
var Concertpost = React.createClass({
  render: function() {
    return (
      <div className="event-widget__post">
      <time className="event-widget__date">
      {this.props.conDate}
      </time>
      <h3 className="event-widget__title">
      {this.props.conName}
      </h3>
      </div>
    );
  }
});

//Data for ConcertDateBoard
var nData = [
  {newsPic: 'http://p1.isanook.com/jo/0/rp/rc/w750h0/ya0xac0m1w0/aHR0cDovL3AxLmlzYW5vb2suY29tL2pvLzAvdWQvNDc2LzIzODE2MjkvdGh1bWIuanBn.jpg', newsName: "คอนเฟิร์ม! Lady Gaga ศิลปินช่วง Half Time Show งาน Super Bowl 51", newsDes: "ปฏิเสธกันไปกันมา สุดท้ายหวยก็มาลงที่ขุ่นแม่ของ Little Monsters จนได้"},
  {newsPic:'http://p1.isanook.com/jo/0/rp/rc/w750h0/ya0xac0m1w0/aHR0cDovL3AxLmlzYW5vb2suY29tL2pvLzAvdWQvNDc2LzIzODE2MDUvdGh1bWIuanBn.jpg', newsName: "Michael Bublé คัมแบ็คกับ “Nobody But Me” เพลงใหม่แต่กินใจเหมือนเดิม", newsDes: "เจ้าพ่อเพลงป็อบแจ๊สเสียงนุ่มขวัญใจชาวไทย เอาเพลงใหม่มาฝ่กแล้ว"},
  {newsPic: 'http://p2.isanook.com/jo/0/rp/rc/w750h0/ya0xac0m1w0/aHR0cDovL3AyLmlzYW5vb2suY29tL2pvLzAvdWQvNDc2LzIzODE1OTcvYWguanBn.jpg', newsName: "อุ่นเครื่อง “ควีน+อดัม แลมเบิร์ต ออน ทัวร์ 2016", newsDes: "ก่อนเปิดการแสดงในบ้านเรา 30 กันยายนนี้"},
  {newsPic: 'http://p1.isanook.com/jo/0/rp/rc/w750h0/ya0xac0m1w0/aHR0cDovL3AxLmlzYW5vb2suY29tL2pvLzAvdWQvNDc2LzIzODE1OTMvdGh1bWIuanBn.jpg', newsName: 'GOT7 เท่ๆ แมนๆ บุกน้ำลุยไฟในเอ็มวี "Hard Carry"', newsDes: "ซิงเกิลแรกจากอัลบั้มเต็มชุดที่ 2 FLIGHT LOG : TURBULENCE"}
];
//NewsBoard
var NewsBoard = React.createClass({
  render: function() {
    var allNews = this.props.data.map(function(news, i) {
      return  (
        <NewsPost newsPicture={news.newsPic} newsName={news.newsName} newsDes={news.newsDes} key={i}></NewsPost>
      );
    });
    return (
      <div className="col span-8">
      <div className="row">
      {allNews}
      </div>
      </div>
    );
  }
});
var NewsPost = React.createClass({
  render: function() {
    return(
      <article className="col span-12">
      <h2 className="post-info post-info--news">
      ข่าว
      </h2>
      <div className="post post-article post-article--list-lg post--list">
      <div className="post-article--list-lg__thumbnail post__thumbnail">

      <img src={this.props.newsPicture} className="post__img" alt="logo" />

      </div>
      <div className="post__detail">
        <h4 className="post__title">
          {this.props.newsName}
        </h4>
        <p className="post__desc">
          {this.props.newsDes}
        </p>
      </div>
      </div>
      </article>
    );
  }
});

//Data for Weeklyband
var wData = [
  {bandPic: 'http://p1.isanook.com/jo/0/rp/rc/w165h165/ya0xac0m1w0/aHR0cDovL3AxLmlzYW5vb2suY29tL2pvLzAvdWQvNDcwLzIzNTQxMjkvYXRvbV8xLmpwZw==.jpg', bandName: 'Atom (อะตอม)'},
  {bandPic: 'http://p1.isanook.com/jo/0/rp/rc/w165h165/ya0xac0m1w0/aHR0cDovL3AxLmlzYW5vb2suY29tL2pvLzAvdWQvNDcwLzIzNTQxMTMvYm9keXNsYW0uanBn.jpg', bandName: 'Bodyslam'},
  {bandPic: 'http://p1.isanook.com/jo/0/rp/rc/w165h165/ya0xac0m1w0/aHR0cDovL3AxLmlzYW5vb2suY29tL2pvLzAvdWQvMTcvODUyNTcvcG9seWNhdC5qcGc=.jpg', bandName: 'Polycat'},
  {bandPic: 'http://p1.isanook.com/jo/0/rp/rc/w165h165/ya0xac0m1w0/aHR0cDovL3AxLmlzYW5vb2suY29tL2pvLzAvdWQvMTcvODUxMDUvbWljaGFlbGJ1YmxlLmpwZw==.jpg', bandName: 'Michael Bublé'},
  {bandPic: 'http://p1.isanook.com/jo/0/rp/rc/w165h165/ya0xac0m1w0/aHR0cDovL3AxLmlzYW5vb2suY29tL2pvLzAvdWQvMTYvODQ2NzcvYnJpdG5leXNwZWFycy5qcGc=.jpg', bandName: 'Britney Spears'},
  {bandPic: 'http://p1.isanook.com/jo/0/rp/rc/w165h165/ya0xac0m1w0/aHR0cDovL3AxLmlzYW5vb2suY29tL2pvLzAvdWQvMTYvODQ2MjUvYmV5b25jZS5qcGc=.jpg', bandName: 'Beyoncé'},
];
//Weeklyband
var WeeklyBand = React.createClass({
  render: function() {
    var bandPostList = this.props.data.map( function(band, i) {
        return (
          <WeeklyBandPost bandPic={band.bandPic} bandName={band.bandName} key={i}></WeeklyBandPost>
        );
    });
    return (
      <div className="weeklyBand">
      <div className="section__header section__header--centered">
      <h5 className="section_title">ศิลปินประจำสัปดาห์<br/>25 กันยายน - 1 ตุลาคม 2559</h5>
      </div>
      <div className="row">
      {bandPostList}
      </div>
      </div>
    );
  }
});
var WeeklyBandPost = React.createClass({
  render: function() {
    return (
      <div className="col span-2 post">
        <div className="post__thumbnail post__thumbnail--circle">
        <img src={this.props.bandPic} className="post__img" alt="logo" />
        </div>
        <div className="post__detail post__detail--artist post__title">
        {this.props.bandName}
        </div>
        </div>
    );
  }
});


module.exports = WebApplication;
